#ifndef CONSULTA
#define CONSULTA

#include "DtFecha.h"
#include <string>

using namespace std;

class Consulta{
        private:
        	DtFecha fechaConsulta;
        	string motivo;
        public:
        	Consulta();
        	Consulta(DtFecha, string);
        	DtFecha getFechaConsulta();
        	string getMotivo();
        	void setFechaConsulta(DtFecha);
        	void setMotivo(string);
        	~Consulta();
};
#endif
