#include "DtConsulta.h"

DtConsulta:: DtConsulta(){

}

DtConsulta:: DtConsulta(DtFecha fechaConsulta, string motivo){
          this->fechaConsulta=fechaConsulta;
          this->motivo=motivo;
}

DtFecha& DtConsulta:: getFechaConsulta(){
          return this->fechaConsulta;
}

string DtConsulta:: getMotivo(){
          return this->motivo;
}

DtConsulta:: ~DtConsulta(){

}
