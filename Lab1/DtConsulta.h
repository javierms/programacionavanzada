#ifndef DTCONSULTA
#define DTCONSULTA

#include "DtFecha.h"
#include <string>

using namespace std;

class DtConsulta{
        private:
        	DtFecha fechaConsulta;
        	string motivo;
        public:
        	DtConsulta();
        	DtConsulta(DtFecha, string);
        	DtFecha& getFechaConsulta();
        	string getMotivo();
        	~DtConsulta();
};
#endif
