#include "DtFecha.h"

DtFecha:: DtFecha(){
        this->dia=0;
        this->mes=0;
        this->anio=0;
}

DtFecha:: DtFecha(int dia, int mes, int anio){
	try{
		if ((dia>31)||(dia<1)||(mes>12)||(mes<1)||(anio<1900)){
			throw std::invalid_argument("Fecha incorrecta");
		}else{
			this->dia=dia;
			this->mes=mes;
			this->anio=anio;
		}
	}catch (std::invalid_argument& e){
		std::cerr << "Error. " << e.what() << '\n';
	}
}

int DtFecha:: getDia(){
        return this->dia;
}

int DtFecha:: getMes(){
        return this->mes;
}

int DtFecha:: getAnio(){
        return this->anio;
}

DtFecha:: ~DtFecha(){
}

bool operator < (DtFecha& f1,DtFecha& f2){
	bool salida;
	if(f1.getAnio()==f2.getAnio()){
		if(f1.getMes()==f2.getMes()){
			if(f1.getDia()==f2.getDia()){
				salida=false;
			}else if(f1.getDia()<f2.getDia()){
				salida=true;
			}else{
				salida=false;
			}
		}else if(f1.getMes()<f2.getMes()){
			salida=true;
		}else{
			salida=false;
		}
	}else if(f1.getAnio()<f2.getAnio()){
		salida=true;
	}else{
		salida=false;
	}

	return salida;
}
