#include "DtGato.h"

DtGato:: DtGato(): DtMascota(){
}

DtGato:: DtGato(string nombre, Genero genero, float peso, float racionDiaria, TipoPelo tipoPelo): DtMascota(nombre, genero, peso, racionDiaria) {
        this->tipoPelo=tipoPelo;
}

TipoPelo DtGato:: getTipoPelo(){
      return this->tipoPelo;
}

DtGato:: ~DtGato(){

}

ostream& operator << (ostream& salida, const DtGato& dtg){
  cout<<(DtMascota) dtg;
  if(dtg.tipoPelo==0){
    cout <<"Pelo: Corto"<< endl;
  }else if(dtg.tipoPelo==1){
    cout <<"Pelo: Mediano"<< endl;
  }else{
    cout <<"Pelo: Largo"<< endl;
  }
	return salida;
}
