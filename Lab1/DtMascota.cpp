#include "DtMascota.h"

DtMascota:: DtMascota(){
}

DtMascota:: DtMascota(string nombre, Genero genero, float peso, float racionDiaria){
        this->nombre=nombre;
        this->genero=genero;
        this->peso=peso;
        this->racionDiaria=racionDiaria;
}

string DtMascota:: getNombre(){
        return this->nombre;
}
Genero DtMascota:: getGenero(){
        return this->genero;
}
float DtMascota:: getPeso(){
        return this->peso;
}
float DtMascota:: getRacionDiaria(){
        return this->racionDiaria;
}

DtMascota:: ~DtMascota(){

}

ostream& operator << (ostream& salida, const DtMascota& dtm){
	cout << "\nNombre: " << dtm.nombre << "\nPeso: " << dtm.peso << "kg" << "\nRacion Diaria: " << dtm.racionDiaria << endl;
  if(dtm.genero==0){
    cout<<"Genero: Macho" << endl;
  }else{
    cout<<"Genero: Hembra" <<endl; 
  }
 	return salida;
}
