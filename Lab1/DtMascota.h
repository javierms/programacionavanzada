#ifndef DTMASCOTA
#define DTMASCOTA

#include "Genero.h"
#include <string>
#include <iostream>

using namespace std;

class DtMascota{
        private:
                string nombre;
                Genero genero;
                float peso;
                float racionDiaria;
        public:
                DtMascota();
                DtMascota(string, Genero, float, float);
                string getNombre();
                Genero getGenero();
                float getPeso();
                float getRacionDiaria();
                virtual ~DtMascota();
//                PREGUNTAR AL PROFE LO DEL VIRTUAL
                friend ostream& operator << (ostream&, const DtMascota&);
};
#endif
