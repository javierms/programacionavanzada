#include "DtPerro.h"

DtPerro:: DtPerro(): DtMascota(){
}

DtPerro:: DtPerro(string nombre, Genero genero, float peso, float racionDiaria, RazaPerro raza, bool vacunaCachorro): DtMascota(nombre, genero, peso, racionDiaria) {
        this->raza=raza;
        this->vacunaCachorro=vacunaCachorro;
}

RazaPerro DtPerro:: getRaza(){
        return this->raza;
}

bool DtPerro:: getVacunaCachorro(){
        return this->vacunaCachorro;
}

DtPerro:: ~DtPerro(){

}

ostream& operator << (ostream& salida, const DtPerro& dtp){
	if (dtp.vacunaCachorro){
		cout << (DtMascota) dtp << "Tiene vacuna del Cachorro: Si" << endl;
	}else{
		cout << (DtMascota) dtp << "Tiene vacuna del Cachorro: No" << endl;
	}
  if(dtp.raza==0){
    cout<<"Raza: Labrador"<<endl;
  }else if(dtp.raza==1){
    cout<<"Raza: Ovejero"<<endl;
  }else if(dtp.raza==2){
    cout<<"Raza: Bulldog"<<endl;
  }else if(dtp.raza==3){
    cout<<"Raza: Pitbull"<<endl;
  }else if(dtp.raza==4){
    cout<<"Raza: Collie"<<endl;
  }else if(dtp.raza==5){
    cout<<"Raza: Pekines"<<endl;
  }else{
      cout<<"Raza: Otra"<<endl;
  }
	return salida;
}
