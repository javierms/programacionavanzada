#include "DtSocio.h"

DtSocio::DtSocio(){}

DtSocio::DtSocio(string ci, string nombre, DtFecha fechaIngreso){
  this->ci=ci;
  this->nombre=nombre;
  this->fechaIngreso=fechaIngreso;
}

string DtSocio::getCi(){
  return this->ci;
}

string DtSocio::getNombre(){
  return this->nombre;
}

DtFecha DtSocio::getFechaIngreso(){
  return this->fechaIngreso;
}

DtSocio:: ~DtSocio(){}
