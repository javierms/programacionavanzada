#ifndef DTSOCIO_H
#define DTSOCIO_H
#include "DtFecha.h"
#include <iostream>

using namespace std;

class DtSocio{
  private:
    string ci;
    string nombre;
    DtFecha fechaIngreso;
  public:
    DtSocio();
    DtSocio(string, string, DtFecha);
    string getCi();
    string getNombre();
    DtFecha getFechaIngreso();
    ~DtSocio();
};

#endif
