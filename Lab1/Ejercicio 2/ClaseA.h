#ifndef CLASEA
#define CLASEA

#include <iostream>

using namespace std;

class ClaseB;
class ClaseC;

class ClaseA{
	private:
		int a;
		ClaseB* cb;
		ClaseC* cc;
	public:
		ClaseA();
		void setA(int);
		int getA();
		void imprimirClase();
        //void mostrarAtributo();
        ~ClaseA();
};

#include "ClaseB.h"
#include "ClaseC.h"

#endif
