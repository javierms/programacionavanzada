#ifndef CLASEB
#define CLASEB

#include "ClaseA.h"
#include "ClaseC.h"

using namespace std;

class ClaseB{
	private:
		int b;
		ClaseA *ca;
		ClaseC *cc;
	public:
		ClaseB();
		void setB(int);
		int getB();
		void imprimirClase();
		//void mostrarAtributo();
		~ClaseB();
};

#endif
