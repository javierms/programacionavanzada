#ifndef CLASEC
#define CLASEC

#include "ClaseA.h"
#include "ClaseB.h"

using namespace std;

class ClaseC{
    private:
        int c;
        ClaseA *ca;
        ClaseB *cb;
	public:
		ClaseC();
		void setC(int);
		int getC();
		void imprimirClase();
		//void mostrarAtributo();
		~ClaseC();
};

#endif
