#include <math.h>
#include <iostream>
#include "ClaseA.h"
#include "ClaseB.h"
#include "ClaseC.h"

using namespace std;

int main(){
	int a=25;
	int b=40;
	int c=b-sqrt(a)+1;
	ClaseA * ca = new ClaseA();
	ClaseB * cb = new ClaseB();
	ClaseC * cc = new ClaseC();
	ca->setA(a);
	ca->imprimirClase();
	cout << ca->getA() << endl;
	delete ca;
	cb->setB(b);
	cb->imprimirClase();
	cout << cb->getB() << endl;
	delete cb;
	cc->setC(c);
	cc->imprimirClase();
	cout << cc->getC() << endl;
	delete cc;
}
