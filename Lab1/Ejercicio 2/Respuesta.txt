﻿1) Las dependencias circulares que fueron necesarias solucionar 
fueron las de las clases A, B y C.

Se declararon dos foward declaration de ClaseB y ClaseC en la ClaseA para solucionar 
el problema de dependencias y el programa compila sin problemas.



2) Una foward declaration es una declaración de un identificador
(que denota una entidad como un tipo, una variable o una función)
para la cual aún no se ha dado una definición completa. 
