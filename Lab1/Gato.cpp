#include "Gato.h"

Gato::Gato(){}

Gato::Gato(string nombre, Genero genero, float peso, float racionDiaria, TipoPelo tipoPelo):Mascota(nombre,genero,peso,racionDiaria){
	this->tipoPelo=tipoPelo;
}

TipoPelo Gato::getTipoPelo(){
	return tipoPelo;
}

void Gato::setTipoPelo(TipoPelo tipoPelo){
	this->tipoPelo=tipoPelo;
}

float Gato::obtenerRacionDiaria(){
	return getPeso()*0.015;
}

Gato::~Gato(){}

