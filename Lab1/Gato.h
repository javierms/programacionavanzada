#ifndef GATO
#define GATO

#include "TipoPelo.h"
#include "Mascota.h"

class Gato :public Mascota{
	private:
		TipoPelo tipoPelo;
	public:
		Gato();
		Gato(string, Genero, float, float, TipoPelo);
		TipoPelo getTipoPelo();
		void setTipoPelo(TipoPelo);
		float obtenerRacionDiaria();
		~Gato();
};
#endif
