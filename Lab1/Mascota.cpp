#include "Mascota.h"

Mascota::Mascota(){};
Mascota::Mascota(string nombre, Genero genero, float peso, float racionDiaria){
  this->nombre=nombre;
  this->genero=genero;
  this->peso=peso;
  this->racionDiaria=racionDiaria;
}

string Mascota::getNombre(){
	return this->nombre;
}

Genero Mascota::getGenero(){
	return this->genero;
}
float Mascota::getPeso(){
	return this->peso;
}

void Mascota::setNombre(string n){
	this->nombre=n;
}
void Mascota::setGenero(Genero g){
	this->genero=g;
}

void Mascota::setPeso(float p){
	this->peso=p;
}

Mascota::~Mascota(){};
