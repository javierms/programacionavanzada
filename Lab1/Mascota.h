#ifndef MASCOTA
#define MASCOTA
#include "Genero.h"

using namespace std;

class Mascota{
	private:
	  string nombre;
	  Genero genero;
	  float peso;
	  float racionDiaria;
	public:
	  Mascota();
	  Mascota(string,Genero,float, float);
	  string getNombre();
	  Genero getGenero();
	  float getPeso();
	  void setNombre(string);
	  void setGenero(Genero);
	  void setPeso(float);
	  virtual float obtenerRacionDiaria()=0;
	  ~Mascota();
};
#endif
