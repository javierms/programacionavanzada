#include "Perro.h"

Perro::Perro(){};
Perro::Perro(string nombre, Genero genero, float peso, float racionDiaria, RazaPerro raza, bool vacunaCachorro):Mascota(nombre,genero,peso,racionDiaria){
	this->vacunaCachorro=vacunaCachorro;
	this->raza=raza;
}

RazaPerro Perro::getRaza(){
	return this->raza;
}

bool Perro::getVacunaCachorro(){
	return this->vacunaCachorro;
}

void Perro::setRaza(RazaPerro raza){
	this->raza=raza;
}

void Perro::setVacunaCachorro(bool vacunaCachorro){
	this->vacunaCachorro=vacunaCachorro;
}

float Perro::obtenerRacionDiaria(){
	return getPeso()*0.025;
}

Perro::~Perro(){};
