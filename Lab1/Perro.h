#ifndef PERRO
#define PERRO
#include "RazaPerro.h"
#include "Mascota.h"

class Perro :public Mascota{
	private:
		RazaPerro raza;
		bool vacunaCachorro;
	public:
		Perro();
		Perro(string, Genero, float, float, RazaPerro, bool);
		RazaPerro getRaza();
		void setRaza(RazaPerro);
		bool getVacunaCachorro();
		void setVacunaCachorro(bool);
		float obtenerRacionDiaria();
		~Perro();
};
#endif
