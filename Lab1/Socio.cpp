#include "Socio.h"

Socio::Socio(){}

Socio::Socio(string ci, string nombre, DtFecha fechaIngreso){
  this->ci=ci;
  this->nombre=nombre;
  this->fechaIngreso=fechaIngreso;
  this->topeMascotas=0;
  this->topeConsultas=0;
}

string Socio::getCi(){
  return this->ci;
}

string Socio::getNombre(){
  return this->nombre;
}

DtFecha Socio::getFechaIngreso(){
  return this->fechaIngreso;
}

int Socio:: getTopeConsultas(){
	return this->topeConsultas;
}

int Socio:: getTopeMascotas(){
	return this->topeMascotas;
}
Mascota* Socio:: getMascota(int pos){
	return this->mascotas[pos];
}

Consulta* Socio:: getConsulta(int pos){
	return this->consultas[pos];
}

void Socio::setCi(string c){
  this->ci=c;
}

void Socio:: setNombre(string nombre){
	this->nombre=nombre;
}
void Socio:: setFechaIngreso(DtFecha fecha){
	this->fechaIngreso=fecha;
}

void Socio:: setTopeConsultas(int tope){
	this->topeConsultas=tope;
}

void Socio:: setTopeMascotas(int tope){
	this->topeMascotas=tope;
}

void Socio:: agregarConsulta(Consulta* consulta){
	this->consultas[this->topeConsultas]=consulta;
	this->topeConsultas++;
}

void Socio:: agregarMascota(Mascota* mascota){
	this->mascotas[this->topeMascotas]=mascota;
	this->topeMascotas++;
}

void Socio:: verMascota (int i){
  if (i<this->topeMascotas){
  	 cout << "\t Mascota: "<< endl;
    cout << "\t\t Nombre: " <<this->mascotas[i]->getNombre() << endl;
    cout << "\t\t Genero: " <<this->mascotas[i]->getGenero() << endl;
    cout << "\t\t Peso: " <<this->mascotas[i]->getPeso() << endl;
  }else{
    cout << "No existe la mascota en la posicion i" << endl;
  }
}

Socio:: ~Socio(){}
