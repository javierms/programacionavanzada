#ifndef SOCIO_H
#define SOCIO_H
#include "Mascota.h"
#include "Consulta.h"

const int MAX_CONSULTA=20;
const int MAX_MASCOTA=10;

class Socio{
  private:
    string ci;
    string nombre;
    DtFecha fechaIngreso;
    int topeConsultas;
    int topeMascotas;
    Consulta* consultas[MAX_CONSULTA];
    Mascota* mascotas[MAX_MASCOTA];
  public:
    Socio();
    Socio(string, string, DtFecha);
    string getCi();
    string getNombre();
    DtFecha getFechaIngreso();
    int getTopeConsultas();
    int getTopeMascotas();
    Mascota* getMascota(int);
    Consulta* getConsulta(int);
    void setCi(string);
    void setNombre(string);
    void setFechaIngreso(DtFecha);
    void setTopeConsultas(int);
    void setTopeMascotas(int);
    void agregarConsulta(Consulta*);
    void agregarMascota(Mascota*);
    void verMascota (int i);
    ~Socio();
};

#endif
