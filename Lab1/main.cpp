//Chequear constantes
//Datatypes
#include "DtFecha.h"
#include "DtConsulta.h"
#include "DtMascota.h"
#include "DtGato.h"
#include "DtPerro.h"

//Clases
#include "Consulta.h"
#include "Mascota.h"
#include "Gato.h"
#include "Perro.h"
#include "Socio.h"
#include "RazaPerro.h"
#include "Genero.h"
#include "TipoPelo.h"

//Bibliotecas
#include <stdexcept>
#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>

const int MAX_SOCIOS=10;

using namespace std;

struct Socios{
	Socio* Socios[MAX_SOCIOS];
	int topeSocios;
	int TopeConsultasAntesFecha;
}ColeccionSocios;

DtFecha fechaActual(){
		time_t now = time(0);
		tm *ltm = localtime(&now);
		DtFecha fecha = DtFecha(ltm->tm_mday, 1 + ltm->tm_mon, 1900 + ltm->tm_year);
		return fecha;
}

DtMascota menuMascota(){
		Genero genero;
		bool generoMal=true;
		string nomMascota, g;
		float peso;
		cout<< "Ingrese el nombre de su mascota: ";
		cin>>nomMascota;
		while (generoMal){
			cout<< "Ingrese su genero (Macho o Hembra): ";
			cin>>g;
			if ((g=="Macho")||(g=="macho")){
				genero=Macho;
				generoMal=false;
			}else if((g=="Hembra")||(g=="hembra")){
				genero=Hembra;
				generoMal=false;
			}else{
				generoMal=true;
			}
		}
		cout << "Ingrese su peso: ";
		cin>>peso;
		DtMascota mascota = DtMascota (nomMascota, genero, peso, 0);
		return mascota;
}

void registrarSocio (string ci, string nombre, DtMascota& dtMascota){
//Registra un socio con su mascota. El valor el atributo racionDiaria se debe setear en 0.
	ColeccionSocios.Socios[ColeccionSocios.topeSocios] = new Socio (ci, nombre, fechaActual());
	try{
		DtPerro& p = dynamic_cast<DtPerro&>(dtMascota);
		Perro* perro = new Perro(p.getNombre(), p.getGenero(), p.getPeso(), 0, p.getRaza(), p.getVacunaCachorro());
		ColeccionSocios.Socios[ColeccionSocios.topeSocios]->agregarMascota(perro);
		}catch(std::bad_cast){
			try{
				DtGato& g = dynamic_cast<DtGato&>(dtMascota);
				Gato* gato = new Gato(g.getNombre(), g.getGenero(), g.getPeso(), 0, g.getTipoPelo());
				ColeccionSocios.Socios[ColeccionSocios.topeSocios]->agregarMascota(gato);
			}catch(std::bad_cast){}
		}
		ColeccionSocios.topeSocios++;
}

void agregarMascota (string ci, DtMascota& dtMascota){
//Agrega una nueva mascota a un socio ya registrado. Si no existe un socio registrado con esa cédula, se levanta una excepción std::invalid_argument.
	int i=0;
	while (((i<ColeccionSocios.topeSocios)&&(ColeccionSocios.Socios[i]->getCi()!=ci))){
			i++;
	}
	try{
		if (i==ColeccionSocios.topeSocios){
			throw std::invalid_argument("No existe el socio con esa cedula.");
		}else{
			try{
					DtPerro& p = dynamic_cast<DtPerro&>(dtMascota);
					Perro* perro = new Perro(p.getNombre(), p.getGenero(), p.getPeso(), 0, p.getRaza(), p.getVacunaCachorro());
					ColeccionSocios.Socios[i]->agregarMascota(perro);
				}catch(std::bad_cast){
					try{
						DtGato& g = dynamic_cast<DtGato&>(dtMascota);
						Gato* gato = new Gato(g.getNombre(), g.getGenero(), g.getPeso(), 0, g.getTipoPelo());
						ColeccionSocios.Socios[i]->agregarMascota(gato);
					}catch(std::bad_cast){}
				}
		}
	}catch (std::invalid_argument& e){
		std::cerr << "Error. " << e.what() << '\n';
	}
}

void ingresarConsulta (string motivo, string ci, DtFecha fecha){
//Crea una consulta con un motivo para un socio. Si no existe un socio registrado con esa cédula, se levanta una excepción std::invalid_argument.
	int i=0;
	while (((i<ColeccionSocios.topeSocios)&&(ColeccionSocios.Socios[i]->getCi()!=ci))){
			i++;
	}
	if (i==ColeccionSocios.topeSocios){
		throw std::invalid_argument("No existe el socio con esa cedula.");
	}else{
		Consulta * consulta = new Consulta (fecha, motivo);
		ColeccionSocios.Socios[i]->agregarConsulta(consulta);
	}
}

DtConsulta** verConsultasAntesDeFecha (DtFecha& fecha, string ciSocio, int& cantConsultas){
//Devuelve las consultas antes de cierta fecha. Para poder implementar esta operación se deberá sobrecargar el operador < (menor que) para el DataType Fecha. El largo del arreglo está dado por el parámetro cantConsultas.
	int i=0, x=0;
	while (((i<ColeccionSocios.topeSocios)&&(ColeccionSocios.Socios[i]->getCi()!=ciSocio))){
			i++;
	}
	DtConsulta** arrConsultas;
	if (i==ColeccionSocios.topeSocios){
		throw std::invalid_argument("No existe el socio con esa cedula.");
	}else{
		if (ColeccionSocios.Socios[i]->getTopeConsultas()<=cantConsultas){
			arrConsultas = new DtConsulta*[ColeccionSocios.Socios[i]->getTopeConsultas()];
			x=ColeccionSocios.Socios[i]->getTopeConsultas();
		}else{
			arrConsultas = new DtConsulta*[cantConsultas];
			x=cantConsultas;
		}
		int j=0;
		while (j<x){
			Consulta* c = ColeccionSocios.Socios[i]->getConsulta(j);
			DtFecha f = DtFecha(c->getFechaConsulta().getDia(), c->getFechaConsulta().getMes(), c->getFechaConsulta().getAnio());
			if (f<fecha){
				arrConsultas[j] = new DtConsulta(c->getFechaConsulta(), c->getMotivo());
				ColeccionSocios.TopeConsultasAntesFecha++;
			}
			j++;
		}
	}
	return arrConsultas;
}


void eliminarSocio(string ci){
//Elimina al socio, sus consultas y sus	mascotas. Si no existe un socio registrado con esa cédula, se levanta una excepción	std::invalid_argument.
	int i=0;
	while (((i<ColeccionSocios.topeSocios)&&(ColeccionSocios.Socios[i]->getCi()!=ci))){
			i++;
	}
	if (i==ColeccionSocios.topeSocios){
		throw std::invalid_argument("No existe el socio con esa cedula.");
	}else{
		if(ColeccionSocios.topeSocios==1){
			ColeccionSocios.Socios[i] = NULL;
		}else{
			ColeccionSocios.Socios[i] = ColeccionSocios.Socios[ColeccionSocios.topeSocios-1];
			ColeccionSocios.Socios[ColeccionSocios.topeSocios-1] = NULL;
		}
		ColeccionSocios.topeSocios--;
	}
}


DtMascota** obtenerMascotas(string ci, int& cantMascotas){
//Devuelve un arreglo con las mascotas del socio. El largo del arreglo está dado por el parámetro cantMascotas. Si no existe un socio registrado con esa cédula, se levanta una excepción std::invalid_argument.
		int i=0, x=0;
		while (((i<ColeccionSocios.topeSocios)&&(ColeccionSocios.Socios[i]->getCi()!=ci))){
				i++;
		}
		DtMascota** arrMascotas;
		if (i==ColeccionSocios.topeSocios){
			throw std::invalid_argument("No existe el socio con esa cedula.");
		}else{
			if (ColeccionSocios.Socios[i]->getTopeMascotas()<=cantMascotas){
				arrMascotas = new DtMascota*[ColeccionSocios.Socios[i]->getTopeMascotas()];
				x=ColeccionSocios.Socios[i]->getTopeMascotas();
			}else{
				arrMascotas = new DtMascota*[cantMascotas];
				x=cantMascotas;
			}
			int j=0;
			while (j<x){
				Perro* p = dynamic_cast<Perro*>(ColeccionSocios.Socios[i]->getMascota(j));
				if (p!=NULL){
					arrMascotas[j] = new DtPerro(p->getNombre(), p->getGenero(), p->getPeso(), p->obtenerRacionDiaria(), p->getRaza(), p->getVacunaCachorro());
				}else{
					Gato* p = dynamic_cast<Gato*>(ColeccionSocios.Socios[i]->getMascota(j));
					arrMascotas[j] = new DtGato(p->getNombre(), p->getGenero(), p->getPeso(), p->obtenerRacionDiaria(), p->getTipoPelo());
				}
				j++;
			}
		}
	return arrMascotas;
}

void verSocios(){
	int i, j;
	for (i=0;i<ColeccionSocios.topeSocios;i++){
		cout<< "Nombre: " << ColeccionSocios.Socios[i]->getNombre()<< endl;
		for (j=0; j< ColeccionSocios.Socios[i]->getTopeMascotas(); j++){
			cout << endl;
			ColeccionSocios.Socios[i]->verMascota(j);
		}
	}
}
int main(){
	int x=10;
	RazaPerro razaP;
	TipoPelo pelo;
	string ci, nombre, optRaza, esPerro, optPelo, vacuna;
	int dia,mes,anio;
	int cantMascotas, cantConsultas;
	bool tieneVacuna, ingRazaMal=true, ingPeloMal=true;
	while(x!=0){
		cout << "\n";
		cout<<"**Bienvenido**"<<endl;
		cout<<"1) Registrar Socio"<<endl;
		cout<<"2) Agregar Mascota"<<endl;
		cout<<"3) Ingresar Consulta"<<endl;
		cout<<"4) Ver Consultas Antes De Fecha"<<endl;
		cout<<"5) Eliminar Socio"<<endl;
		cout<<"6) Obtener Mascotas"<<endl;
		cout<<"7) Ver Socios con sus mascotas"<<endl;
		cout<<"0) Salir"<< endl;
		cout << endl;
		cout << "Opcion: ";
		cin>>x;
		cout << endl;
		switch (x){
			case 1:{
				cout<<"-> Registrar Socio"<<endl;
				ingRazaMal=true;
				ingPeloMal=true;
				//Pedimos los datos suyos y de su mascota
				cout<< "Ingrese su ci: ";
				cin>>ci;
				cout<< "Ingrese su nombre: ";
				cin>>nombre;
				DtMascota dtm = menuMascota();
				//Preguntamos si su mascota es perro o gato
				cout<< "¿Su mascota es un perro? s/n: ";
				cin>>esPerro;
				//En el caso de que sea perro
				if (esPerro == "s"){
					//Preguntamos su raza y su vacuna
					while (ingRazaMal){
						cout<< "Ingrese su raza (Labrador, Ovejero, Pitbull, Bulldog, Collie, Pekines o otro): ";
						cin>>optRaza;
						if (optRaza=="labrador" or optRaza=="Labrador"){
								razaP= labrador;
								ingRazaMal=false;
						}else if (optRaza=="ovejero" or optRaza=="Ovejero"){
							razaP= ovejero;
							ingRazaMal=false;
						}else if (optRaza=="pitbull" or  optRaza=="Pitbull"){
							razaP= pitbull;
							ingRazaMal=false;
						}else if(optRaza=="bulldog" or optRaza=="Bulldog"){
							razaP= bulldog;
							ingRazaMal=false;
						}else if(optRaza=="collie" or optRaza=="Collie"){
							razaP= collie;
							ingRazaMal=false;
						}else if(optRaza=="pekines" or optRaza=="Pekines"){
							razaP= pekines;
							ingRazaMal=false;
						}else if (optRaza=="otro" or optRaza=="Otro"){
							razaP= otro;
							ingRazaMal=false;
						}else{
							cout << "Genero incorrecto" << endl;
							ingRazaMal=true;
						}
					}
					cout<< "¿Le dieron la vacuna del cachorro a su perro? (s/n): ";
					cin>>vacuna;
					if(vacuna=="s"){
						tieneVacuna=true;
					}else if (vacuna=="n"){
						tieneVacuna=false;
					}
					DtPerro dtp = DtPerro(dtm.getNombre(), dtm.getGenero(), dtm.getPeso(), dtm.getRacionDiaria(), razaP, tieneVacuna);
					registrarSocio(ci, nombre, dtp);
				}else if (esPerro=="n"){ //Es Gato
					while (ingPeloMal){
						cout << "Ingrese la medida de su pelo (Corto, Mediano o Largo): ";
						cin>> optPelo;
						if((optPelo=="Corto" )|| (optPelo=="corto")){
								pelo= Corto;
								ingPeloMal=false;
						}else if ((optPelo=="Mediano")|| (optPelo=="mediano")){
								pelo= Mediano;
								ingPeloMal=false;
						}else if ((optPelo=="Largo")|| (optPelo=="largo")){
								pelo= Largo;
								ingPeloMal=false;
						}else{
								cout << "Tipo de pelo invalido" << endl;
								ingPeloMal=true;
						}
					}
					DtGato dtg = DtGato(dtm.getNombre(), dtm.getGenero(), dtm.getPeso(), dtm.getRacionDiaria(), pelo);
					registrarSocio(ci, nombre, dtg);
				}

			break;
			}
			case 2:{
				cout<<"-> Agregar Mascota"<<endl;
				ingRazaMal=true;
				ingPeloMal=true;
				cout<< "Ingrese su ci: ";
				cin>>ci;
				DtMascota dtm = menuMascota();
				//Preguntamos si su mascota es perro o gato
				cout<< "¿Su mascota es un perro? s/n: ";
				cin>>esPerro;
				//En el caso de que sea perro
				if (esPerro == "s"){
					//Preguntamos su raza y su vacuna
					while (ingRazaMal){
						cout<< "Ingrese su raza (Labrador, Ovejero, Pitbull, Bulldog, Collie, Pekines o otro): ";
						cin>>optRaza;
						if (optRaza=="labrador" or optRaza=="Labrador"){
								razaP= labrador;
								ingRazaMal=false;
						}else if (optRaza=="ovejero" or optRaza=="Ovejero"){
							razaP= ovejero;
							ingRazaMal=false;
						}else if (optRaza=="pitbull" or  optRaza=="Pitbull"){
							razaP= pitbull;
							ingRazaMal=false;
						}else if(optRaza=="bulldog" or optRaza=="Bulldog"){
							razaP= bulldog;
							ingRazaMal=false;
						}else if(optRaza=="collie" or optRaza=="Collie"){
							razaP= collie;
							ingRazaMal=false;
						}else if(optRaza=="pekines" or optRaza=="Pekines"){
							razaP= pekines;
							ingRazaMal=false;
						}else if (optRaza=="otro" or optRaza=="Otro"){
							razaP= otro;
							ingRazaMal=false;
						}else{
							cout << "Genero incorrecto" << endl;
							ingRazaMal=true;
						}
					}
					cout<< "�Le dieron la vacuna del cachorro a su perro? (s/n): ";
					cin>>vacuna;
					if(vacuna=="s"){
						tieneVacuna=true;
					}else if (vacuna=="n"){
						tieneVacuna=false;
					}
					DtPerro dtp = DtPerro(dtm.getNombre(), dtm.getGenero(), dtm.getPeso(), dtm.getRacionDiaria(), razaP, tieneVacuna);
					agregarMascota(ci, dtp);
				}else if (esPerro=="n"){ //Es Gato
					while (ingPeloMal){
						cout << "Ingrese la medida de su pelo (Corto, Mediano o Largo): ";
						cin>> optPelo;
						if((optPelo=="Corto" )|| (optPelo=="corto")){
								pelo= Corto;
								ingPeloMal=false;
						}else if ((optPelo=="Mediano")|| (optPelo=="mediano")){
								pelo= Mediano;
								ingPeloMal=false;
						}else if ((optPelo=="Largo")|| (optPelo=="largo")){
								pelo= Largo;
								ingPeloMal=false;
						}else{
								cout << "Tipo de pelo invalido" << endl;
								ingPeloMal=true;
						}
					}
					DtGato dtg = DtGato(dtm.getNombre(), dtm.getGenero(), dtm.getPeso(), dtm.getRacionDiaria(), pelo);
					agregarMascota(ci, dtg);
				}

			break;
			}
			case 3:{
				cout<<"-> Ingresar Consulta"<<endl;
				cout<<"Ingrese su cedula: ";
				cin>>ci;
				string motivo;
				cout<<"Ingrese un motivo: ";
				cin>>motivo;
				cout<<"Ingrese dia: ";
				cin>>dia;
				cout<<"Ingrese mes: ";
				cin>>mes;
				cout<<"Ingrese año: ";
				cin>>anio;
				DtFecha f1 = DtFecha(dia,mes,anio);
				ingresarConsulta (motivo, ci, f1);
			break;
			}
			case 4:{
				cout<<"-> Ver Consultas Antes De Fecha"<<endl;
				cout<<"Ingrese su cedula: ";
				cin>>ci;
				cout<<"Ingrese dia: ";
				cin>>dia;
				cout<<"Ingrese mes: ";
				cin>>mes;
				cout<<"Ingrese año: ";
				cin>>anio;
				cout<<"Ingrese cantidad de consultas: ";
				cin>>cantConsultas;
				DtFecha f1 = DtFecha(dia,mes,anio);
				DtConsulta** dt=verConsultasAntesDeFecha(f1,ci,cantConsultas);

				int i=0;
				while (ColeccionSocios.Socios[i]->getCi()!=ci){
					i++;
				}
				int j=0;
				while (j<ColeccionSocios.TopeConsultasAntesFecha){
					cout << endl;
					cout << "Consulta " << j << endl;
					DtConsulta* c = dt[j];
					cout << "Motivo: " << c->getMotivo() << endl;
					j++;
				}
				ColeccionSocios.TopeConsultasAntesFecha=0;
			break;
			}
			case 5:{
				cout<<"-> Eliminar socio"<<endl;
				cout<<"Ingrese su ci: ";
				cin>>ci;
				eliminarSocio(ci);
			break;
			}
			case 6:{
				cout<<"-> Obtener Mascotas"<<endl;
				cout<<"Ingrese su ci: ";
				cin>>ci;
				cout<<"Ingrese cantidad de mascotas: ";
				cin>>cantMascotas;

				DtMascota** dt=obtenerMascotas(ci, cantMascotas);

				int i=0;
				while (ColeccionSocios.Socios[i]->getCi()!=ci){
					i++;
				}
				int j=0, x=0;
				if (ColeccionSocios.Socios[i]->getTopeMascotas()<=cantMascotas){
					x=ColeccionSocios.Socios[i]->getTopeMascotas();
				}else{
					x=cantMascotas;
				}
				while (j<x){
					cout << endl;
					cout << "Mascota " << j << endl;
					DtPerro* p = dynamic_cast<DtPerro*>(dt[j]);
					if (p!=NULL){
						cout << *p << endl;
					}else{
						DtGato* g = dynamic_cast<DtGato*>(dt[j]);
						cout << *g << endl;
					}
					j++;
				}
			break;
			}
			case 7:{
				cout<<"-> Ver Socios con sus mascotas: "<<endl;
				verSocios();
			}
		}
	}
		return 0;
}
